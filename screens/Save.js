import { Button, StyleSheet, Text, View, Image, TouchableOpacity,SafeAreaView, TextInput } from 'react-native'
import React, { useState } from 'react'
import Header from '../component/Components/Header'
import {firebase} from '../component/Firebase/config'
// require("firebase /firestore")
// require("firebase/firebase-storage")
export default function Save(props, {navigation}) {
  console.log('i m in save')

  // const data = data.params
  const Name='item-'+ new Date().toISOString();

  console.log(props.route.params.image)
  const [caption, setCaption] = useState("") 
  const [loading, setLoading] = useState();
  const uploadImage = async() => {
    const uri = props.route.params.image;   
    const childPath = 'image/'
    console.log(childPath)
    const response = await fetch(uri);
    const blob = await response.blob();
    const task = firebase
      .storage()
      .ref()
      .child(childPath+Name)
      .put(blob);
      // setLoading(true)
    const taskProgress = snapshot => {
      console.log('transferred: ${snapshot.bytesTransferred}')
    }
    const taskCompleted = ()=> {
      task.snapshot.ref.getDownloadURL().then((snapshot) => {
        savePostData(snapshot);
        console.log(snapshot)
      })
    }
    const taskError = snapshot => {
      console.log(snapshot)
    }
    task.on("state_changed", taskProgress, taskError, taskCompleted);
  }
  const savePostData =  (downloadURL) => {
    firebase.firestore()
      .collection('breakfast')
      .add({
        downloadURL,
        caption,
        creation: firebase.firestore.FieldValue.serverTimestamp()
      })
      .then((function () {
        // props.navigation.popToTop()
        alert('Successfully uploaded')
        props.navigation.navigate('Page')
        
      }))
  }
// setLoading(false)
  return (
    <View style={{ flex: 1, margin: 15, marginTop:45 }}>
      {/* <TouchableOpacity onPress={()=>navigation.replace("Add")}>
        <Image
            style={styles.imageBack}
            source={require('../../../assets/arrow_back.png')} />
      </TouchableOpacity> */}
      <Header>SaveScreen</Header>
      <Image source={{uri: props.route.params.image}}/>
      {/* <TextInput
        placeholder='Write a caption...'
        onChangeText={(caption) => setCaption(caption)}
      /> */}
      <Button 
        title='Save'
        color={'#2f3d4f'} 
        onPress={() => uploadImage()}
        // loading = {loading}
        />
    </View>
  )
}
const styles = StyleSheet.create({
  imageBack: {
    width: 25,
    height: 25,
},
})
