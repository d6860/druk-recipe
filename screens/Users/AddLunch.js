import { StyleSheet, Text,Image,Keyboard,ScrollView, View,TouchableOpacity} from 'react-native'
import React, { useState } from 'react';
  import { Dropdown } from 'react-native-element-dropdown';
  import AntDesign from 'react-native-vector-icons/AntDesign';
  import { Ionicons } from '@expo/vector-icons'; 
  import {firebase} from '../../component/Firebase/config.js'
  import { SafeAreaView } from 'react-native-safe-area-context';
  import { TextInput } from 'react-native-paper';
  
  import ButtonOne from '../../component/Components/ButtonOne';
import UploadImage from '../../component/Components/Image.js';


const AddLunch = (props,{navigation}) => {
  const Name='item-'+ new Date().toISOString();

  const [value, setValue] = useState(null);
  // recipes collection reference
  const [tittle, setTittle] = useState('');
  const [process, setProcess] = useState('');
  const [ingredient, setIngredient] = useState('');
  const [lunch, setLunch] = useState([]);
  const tittleRef = firebase.firestore().collection('lunch');
  
  const uploadImage = async() => {
    console.log("working upload image")
    const uri = props.route.params.image;
    const childPath = 'items/'
    const response = await fetch(uri);
    const blob = await response.blob()
    const task = firebase
      .storage()
      .ref()
      .child(childPath+Name)
      .put(blob);
    
      const taskProgress = snapshot => {
        console.log('transferred: ${snapshot.bytesTransferred}')
      }
      const taskCompleted = ()=> {
        task.snapshot.ref.getDownloadURL().then((snapshot) => {
          AddLunch(snapshot);
        })
      }
      const taskError = snapshot => {
        console.log(snapshot)
      }
      task.on("state_changed", taskProgress, taskError, taskCompleted);
    }


  
  const AddLunch = (downloadURL) => {
    if (tittle && tittle.length > 0) {
      const timestamp = firebase.firestore.FieldValue.serverTimestamp();
      console.log('working add lunch')
        const data = {
            downloadURL: downloadURL,
            top: tittle,
            steps:process,
            needs:ingredient,
            createdAt: timestamp
        };
        
        tittleRef
            .add(data)
            .then(() => {
                setTittle('');
                Keyboard.dismiss();
                props.navigation.navigate('LoginHomepage') 
            })
            .catch((error) => {
                alert(error);
            })
            alert('Successfully added')
          }
        }
  
  return (
        
    <SafeAreaView style={{maxheight:'100%', backgroundColor:'white'}}>
        <ScrollView>
            <View>    
                  <View style={{backgroundColor:'#2f3d4f', flexDirection:'row',alignItems:'center'}}>
                <View style={{marginTop:20, flexDirection:'row'}}>
                
                      <TouchableOpacity onPress={()=>props.navigation.replace("LoginHomepage")}>
                      
                      <Ionicons name="md-chevron-back-outline" size={24} color="white"  /> 
                      </TouchableOpacity>
                
                  
                         <Text style={{marginLeft:110,marginBottom:15, fontSize:20,color:'white'}}>AddLunch</Text>
                  </View>

                </View>
                  
                    <View  style={{width:800,height:800}}>
                      
                    <View style={{}}>
                    <View style={styles.view}>
                          <Text>Tittle</Text>
                          </View >
                      <View style={styles.input}>
                      <TextInput
                                numberOfLines={1}
                                multiline={true}
                                onChangeText={(top) => setTittle(top)}
                                    value={tittle}
                                   
                                style={{
                                    width:275, 
                                    borderWidth:1,
                                    borderColor:'black',
                                    backgroundColor:'white',
                                    marginLeft:20,
                                    marginBottom:5,
                                    textAlignVertical: "top",
                                   
                              }}
                            
                            />
                      </View>
                           
                    </View>
                     
                          
                      
                        
                      
                    
                    <View style={{flexDirection:'row'}}>
                        
                    
                    </View>
                    <View></View>
                    <View style={styles.view}>
                          <Text>Ingredients</Text>
                     </View>
                     <View style={styles.input}>
                     <TextInput 
                              numberOfLines={5}
                              multiline={true}
                              onChangeText={(steps) => setProcess(steps)}
                                    value={process}
                              style={{
                                width:275,
                                borderWidth:1,
                                borderColor:'black',
                                backgroundColor:'white',
                                marginLeft:20,
                                textAlignVertical: "top",
                            }}
                            
                            /> 
                     </View>
                           
                        
                   
                    <View style={styles.view}>
                          <Text>Process </Text>
                    </View>
                    <View style={styles.input}>
                    <TextInput
                                  numberOfLines={5}
                                  multiline={true}
                                  onChangeText={(needs) => setIngredient(needs)}
                                    value={ingredient}
                                  
                                    
                                    
                                  style={{
                                      width:275,
                                      borderWidth:1,
                                      borderColor:'black',
                                      backgroundColor:'white',
                                      margin:10,
                                      marginLeft:20,
                                      textAlignVertical: "top",
                                    
                                      
                                      
                                      
                                  }}
                            />
                            
                        
                    </View>
                    {/* <View style={styles.view}>
                      <TouchableOpacity onPress={()=>navigation.replace("Pick")}>
                       <Text>Add Image</Text>
                      </TouchableOpacity>
                          
                     </View> */}
                           
                        
                   
                    <View style={styles.drop}>
                    
                        
                        {/* <UploadImage></UploadImage> */}
                        
                    </View >
                    <View  style={{flexDirection:'row',}}>
                      <ButtonOne  onPress={()=>props.navigation.replace("LoginHomepage")}> CANCEL  </ButtonOne>
                      <ButtonOne  onPress={uploadImage}>ADD </ButtonOne>
                      </View>
                    </View>
            </View>
            <View style={{height:500}}>

            </View>
            </ScrollView>
            </SafeAreaView>
          )
        }
        

const styles = StyleSheet.create({
    view:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:35,
        
        

    },
    input:{
      flexDirection:'row',
      marginTop:20,
      marginLeft:20,
      
    },
    drop:{
      flexDirection:'column'  
    },
    dropdown: {
      margin: 16,
      height: 50,
      borderBottomColor: 'gray',
      borderBottomWidth: 0.5,
    
    },
    icon: {
      marginRight: 5,
    },
      placeholderStyle: {
      fontSize: 16,
    },
    selectedTextStyle: {
      fontSize: 16,
    },
    iconStyle: {
      width: 20,
      height: 20,
    },
    inputSearchStyle: {
      height: 40,
      fontSize: 16,
    },
    
    
})
export default AddLunch
