
import { StyleSheet, Text,TouchableOpacity, View,Image } from 'react-native'
import React from 'react'
import { Ionicons } from '@expo/vector-icons'; 

const AboutPage = ({navigation}) => {
  return (
    <View style={{backgroundColor:'#A2B9D1',height:'100%'}}>
      <View style={{backgroundColor:'#2f3d4f', flexDirection:'row',alignItems:'center'}}>
                <View style={{marginTop:20, flexDirection:'row',marginLeft:'6%'}}>
                
                      <TouchableOpacity onPress={()=>navigation.replace("Page")}>
                      
                      <Ionicons name="md-chevron-back-outline" size={24} color="white"  /> 
                      </TouchableOpacity>
                
                  
                         <Text style={{marginLeft:120,marginBottom:15, fontSize:20,color:'white'}}>About Us </Text>
                  </View>
                  </View>
        
        <View style={{ justifyContent:'center',alignItems:'center',marginTop:1,backgroundColor:'white',width:390,height:400, marginLeft:0, borderBottomStartRadius:300,borderBottomEndRadius:0 }}>
          <View style = {{flexDirection:'row'}}>
               <Image  style={{width:200,height:200,marginLeft:40,borderRadius:100, marginBottom:10}} source={require('../../assets/assets/nose1.png')}>

              </Image>
              <Text>
              {'\n'}      </Text>
              
          </View>
       
      {/* <View style={{ justifyContent:'center',alignItems:'center',marginTop:20,backgroundColor:'white',width:300,height:200, marginLeft:10 }}>

      </View> */}
        </View>
        <View style={{height:30}}></View>
        <Text style={{fontWeight:'bold',fontSize:25,marginLeft:100,color:'black'}}> About us</Text>
        <Text style={{color:'black',marginTop:10, marginLeft:'20%',fontSize:17}}>
                
              To provide recipes of  
              Bhutanese cuisine to   
              those people who love 
              to explore about foods  
              and those who lack cooking skills.

              </Text>
         
    </View>
  )
}

export default AboutPage

const styles = StyleSheet.create({})