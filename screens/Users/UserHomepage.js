import React,{useState} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,TextInput, View, ScrollView,SafeAreaView } from "react-native";
import Action from "../../component/Components/Action";
import Icon from 'react-native-vector-icons/Ionicons';
import { FontAwesome5 } from '@expo/vector-icons';
import { StatusBar } from "expo-status-bar";
const Page = ({navigation}) => {
  const [s, setS] = useState('')
  const Sen = () => {
    if (s=='') {
      alert('search cannot be empty!')
      return false;
    }
    else{
      navigation.navigate('Search',s)
    }
  }
  return(
  <SafeAreaView style={{maxHeight:'100%',}}>
    <StatusBar style="light"/>
      <View style={{backgroundColor:'#2f3d4f',height:80}}>
    
        <View style={{marginTop:30, flexDirection: 'row',backgroundColor:'white',width:'95%',marginLeft:10,borderRadius:10}}>
            <View style={styles.searchContainer}>
              <TextInput
              autoCapitalize='none'
              Style={{color:'white'}}
              onChangeText={(text)=> setS(text)}
              placeholder='search'></TextInput>
            </View>
            <TouchableOpacity style={{marginRight:20,marginTop:10}} onPress={Sen}>
            <FontAwesome5 name="search" size={20} color="#2f3d4f" />
            </TouchableOpacity>
        </View>
       
      </View>
   
      <ScrollView>
        
        <View style={{marginTop:0, backgroundColor:'#A2B9D1'}} >
        
          <View style={styles.container}>
            
                <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/break.png')} resizeMode="cover" style={styles.image}>
                  <Text style={{ color: "white",
                                  fontSize: 28,
                                  lineHeight: 84,
                                  fontWeight: "bold",
                                  marginTop:20,
                                  marginLeft:20,
                                  alignItems:'center',
                                  marginLeft:100}}>BREAKFAST</Text>
                       <View style={{flexDirection:'row'}}>
                         <View>
                                   <TouchableOpacity onPress={()=>navigation.replace("Rating")}>
                              <View style={{marginLeft:120,
                                            marginTop:20}}      >
                              <FontAwesome5 name="star" size={24} color="white" />
                                </View>
                        </TouchableOpacity>
                         </View>
                      
                          <TouchableOpacity   onPress={()=>navigation.replace("BreakfastItem1")}>
                              <View style={{marginLeft:20,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>navigation.replace("BreakFast")}>
                              <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                        </TouchableOpacity>
                        
                       </View>
                 
                </ImageBackground>
            
           
          </View>

          <View style={styles.container}>
          
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/lunch.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>LUNCH</Text>
              <View style={{flexDirection:'row'}}>
              <View>
                                   <TouchableOpacity onPress={()=>navigation.replace("Rating")}>
                              <View style={{marginLeft:120,
                                            marginTop:20}}      >
                              <FontAwesome5 name="star" size={24} color="white" />
                                </View>
                        </TouchableOpacity>
                         </View>
                    
                      <TouchableOpacity   onPress={()=>navigation.replace("LunchItem")}>
                          <View style={{marginLeft:20,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                           </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("Lunch")}>
                          <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                          </View>
                    </TouchableOpacity>
                  </View>
              
            </ImageBackground>
          
            
          </View>

          <View style={styles.container}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/dinner.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>DINNER</Text>
              <View style={{flexDirection:'row'}}>
              <View>
                                   <TouchableOpacity onPress={()=>navigation.replace("Rating")}>
                              <View style={{marginLeft:120,
                                            marginTop:20}}      >
                              <FontAwesome5 name="star" size={24} color="white" />
                                </View>
                        </TouchableOpacity>
                         </View>
                      
                      <TouchableOpacity   onPress={()=>navigation.replace("DinnerItem")}>
                              <View style={{marginLeft:20,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("Dinner")}>
                              <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                  </View>
            </ImageBackground>
            
           
          </View>

          <View style={styles.container}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9, blurRadius:1}} source={require('../../assets/assets/snacks.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>SNACKS</Text>
              <View style={{flexDirection:'row'}}>
              <View>
                                   <TouchableOpacity onPress={()=>navigation.replace("Rating")}>
                              <View style={{marginLeft:120,
                                            marginTop:20}}      >
                              <FontAwesome5 name="star" size={24} color="white" />
                                </View>
                        </TouchableOpacity>
                         </View>
                      
                      <TouchableOpacity   onPress={()=>navigation.replace("SnacksItem")}>
                              <View style={{marginLeft:20,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("Snacks")}>
                              <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                  </View>
            </ImageBackground>
          
            
          </View>

          <View style={styles.container}>
          
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/drinks.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>Drinks</Text>
              
              <View style={{flexDirection:'row'}}>
              <View>
                                   <TouchableOpacity onPress={()=>navigation.replace("Rating")}>
                              <View style={{marginLeft:120,
                                            marginTop:20}}      >
                              <FontAwesome5 name="star" size={24} color="white" />
                                </View>
                        </TouchableOpacity>
                         </View>
                      
                      <TouchableOpacity   onPress={()=>navigation.replace("DrinksItem")}>
                              <View style={{marginLeft:20,
                                            marginTop:20}}      >
                              <FontAwesome5 name="eye" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace("Drinks")}>
                              <View style={{marginLeft:30,
                                            marginTop:20}}      >
                              <FontAwesome5 name="comment" size={24} color="white" />
                                </View>
                    </TouchableOpacity>
                  </View>
              
            </ImageBackground>
  
            <View style={{height:30}} >

              </View>
          </View>
         
          
        </View>
      </ScrollView>
      
           <View style={{width:'100%', height:'6%',backgroundColor:'#2f3d4f', flexDirection:'row'}} >
          
             <TouchableOpacity>
                      <View style={{marginLeft:30,
                                    marginTop:10}}      >
                      <FontAwesome5 name="home" size={24} color="white" />
                        </View>
             </TouchableOpacity >
            <TouchableOpacity onPress={()=>navigation.replace("AboutPage")}>
                <View style={{marginLeft:80,
                              marginTop:10}}      >
                <FontAwesome5 name="info-circle" size={24} color="white" />
                  </View>
            </TouchableOpacity>
            <TouchableOpacity  onPress={()=>navigation.replace("UserLoginScreen")}>
                    <View style={{marginLeft:80,
                                  marginTop:10}}      >
                    <FontAwesome5 name="comment" size={24} color="white" />
                      </View>
           
            </TouchableOpacity>
            
            <TouchableOpacity  onPress={()=>navigation.replace("UserLoginScreen")}>
            
                    <View style={{marginLeft:50,
                                  marginTop:10}}      >
                    <FontAwesome5 name="plus" size={24} color="white" />
                   
                      </View>
           
            </TouchableOpacity>
            
           
          </View>

        
  </SafeAreaView>

)
 };

const styles = StyleSheet.create({
  container: {
   marginTop: 10,
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '95%',
    height: 170,
    marginLeft:20,
    overflow:'hidden',
  },
  text: {
    color: "white",
    fontSize: 28,
    lineHeight: 84,
    fontWeight: "bold",
    marginTop:20,
    marginLeft:20,
    alignItems:'center',
    marginLeft:130
    
  },
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  searchContainer: {
    height: 40,
    borderRadius: 10,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
    
},
});

export default Page;