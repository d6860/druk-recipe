import React,{useState,useEffect} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView, FlatList } from "react-native";
import Action from "../../component/Components/Action";
import TextInput from "../../component/Components/TextInput";
import {firebase} from '../../component/Firebase/config';
import Icon from 'react-native-vector-icons/Ionicons';
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import ButtonOne from '../../component/Components/ButtonThree';

export default function UserDrinksItem ({navigation}) {
  

  const [tittle, setTittle] = useState('');
  const[user]=useState('');
  const [drinks, setDrinks] = useState([]);
  const tittleRef = firebase.firestore().collection('drinks'); 
  
  useEffect(() => {

    tittleRef
        .orderBy('createdAt', 'desc')
        .onSnapshot(
            querySnapshot => {
                const newDrinks = []
                querySnapshot.forEach((doc)=>{
                  const { top,needs,steps, downloadURL} = doc.data()

                  newDrinks.push({
                    id: doc.id,
                    top,
                    needs,
                    steps,                  
                    downloadURL,

                  })
                })
                setDrinks(newDrinks)
            },
            error => {

                console.error(error);
            }
        )
}, []);

const renderTittle = ({ item }) => {
    <View style={styles.tittleContainer} >
            
              <Text style={styles.tittleText}>
                  {item.text[0].toUpperCase() + item.text.slice(1)}
              </Text>
          

      </View>
  return (

      <View style={styles.tittleContainer} >
            
              <Text style={styles.tittleText}>
                  {item.text[0].toUpperCase() + item.text.slice(1)}
              </Text>
          

      </View>
  )
}
return (

  <SafeAreaView style={{backgroundColor:'#EEECED'}}>
      
      <View style={{backgroundColor:'#2f3d4f', }}>
     
      <View style={{marginTop:25,}}>
        <View style={{marginTop:10,flexDirection:'row',}} >
        <TouchableOpacity onPress={()=>navigation.replace("LoginHomepage")}>
           
           <Ionicons name="md-chevron-back-outline" size={24} color="white" /> 
           </TouchableOpacity>
        <Text style={{marginLeft:110,marginBottom:15, fontSize:20,color:'white'}}>Drinks</Text>
        </View>
     
      </View>
      
        </View>
        
      
    
    <View style={{height:'100%'}}>

    {drinks.length > 0 && (
              <View style={styles.listContainer}>
                  <FlatList
                      data={drinks}
                      
                      renderItem={({ item }) => {
                        return(
                             
                            <View key={item.id} style={styles.f}>
                              <Image  style={{width:30,height:30,marginLeft:15,width:220,height:170,borderRadius:10,marginTop:20}} source={{uri: item.downloadURL}} />        
                              <View style={styles.g}>
                                <Text style={{ color:'#2f3d4f',marginLeft:100,fontSize:20,marginTop:30}}>{item.top}</Text>
                               
                                  <View  style={{marginLeft:0,marginTop:30,}}>
                                  <ButtonOne onPress={()=>navigation.navigate('UserDrinksItem1',item)}>VIEW</ButtonOne>
                                  </View>  
                                  
                                  </View>
                                               
                              </View> 
                              
                                  
                         )
                       }} />
                <View style={{height:200}}>
                              </View>
              </View>
               
          )}
    </View>
  </SafeAreaView>
)
}

const styles = StyleSheet.create({
  container: {
   marginTop: 10,
  },
  // image: {
  //   flex: 1,
  //   justifyContent: "center",
  //   width: '95%',
  //   height: 200,
  //   marginLeft:20,
  //   overflow:'hidden',
  
  // text: {
  //   color: "white",
  //   fontSize: 28,
  //   lineHeight: 84,
  //   fontWeight: "bold",
  //   marginTop:130
    
  
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  tittleContainer: {
    borderBottomColor: 'black',
    borderWidth: 1,
    flex:1 ,
    flexDirection: 'column',
    height: 100,
    width: 100,
    margin: 5,
    backgroundColor: 'blue'
  },
  tittleText: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  listContainer: {
    height: '100%',
    marginTop:10,
    
    
  },
  place:{
        
  },
  f:{
    backgroundColor:'#A2B9D1',
    height: 350,
    width: 250,
    marginLeft:70,
    borderTopEndRadius:10,
    borderTopStartRadius:10,
    marginBottom:80
    

  },
  g:{
    backgroundColor:'#ADD8E6',
    height:200,
    width: 250,
    borderTopEndRadius:20,
    borderTopStartRadius:20,
    borderBottomEndRadius:10,
    borderBottomStartRadius:10,
    marginTop:10
    
  }
  
});



