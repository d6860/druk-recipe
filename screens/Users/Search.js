import React,{useState,useEffect} from "react"
import Header from '../../component/Components/Header'
import {Text,View,StyleSheet, Image, Dimensions, TouchableOpacity, FlatList} from 'react-native'
import { theme } from "../../core/helpers/theme"
import {firebase} from '../../component/Firebase/config';
import { ActivityIndicator, Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons'
const width = Dimensions.get("screen").width/2-30
export default function Search({navigation, route}){
    const s = route.params
    const [loading, setLoading] = useState(true)
    const [users, setUsers] = useState([])
    const [users1, setUsers1] = useState([])
    const [users2, setUsers2] = useState([])
    const todoRefB = firebase.firestore().collection('breakfast')
    const todoRefL = firebase.firestore().collection('Lunch')
    const todoRefD = firebase.firestore().collection('dinner')
    const todoRefS = firebase.firestore().collection('snacks')
    const todoRefK = firebase.firestore().collection('drinks')
    
    useEffect(()=>{
        todoRefB
        .onSnapshot(
          query  => {
            const users = []
            query.forEach((doc)=> {
                const {top,needs,steps, downloadURL,} = doc.data()
                const c = top.charAt(0)
                if(c==s){
                    users.push({
                    id: doc.id,
                    steps,
                    needs,
                    top,
                    downloadURL,
                    
                    
                    })
                }
              })
              setUsers(users)
              setLoading(false)
            }
                   
        )
    }, [])
    useEffect(()=>{
        todoRefL
        .onSnapshot(
          query  => {
            const users = []
            query.forEach((doc)=> {
                const {top,needs,steps, downloadURL,} = doc.data()
                const c = top.charAt()
                if(c==s){
                    users.push({
                    id: doc.id,
                    steps,
                    needs,
                    top,
                    downloadURL,
                    
                    
                    })
                }
              })
              setUsers2(users)
              setLoading(false)
            }
                   
        )
    }, [])
    const Refresh=()=>{
        const clonedArr = [...users2];
        setUsers(clonedArr)
        const t = [...users]
        setUsers1
        (t)
    }
    
    return (
        <View style={style.container}>
            <View style={{width: '100%',paddingHorizontal:10,flexDirection:'row', justifyContent:'space-between'}}>
                <TouchableOpacity style={{marginTop: 40, flexDirection: 'row'}} onPress={()=>navigation.replace('Page')}>
                    <Icon name='keyboard-arrow-left' size={20} />
                    <Text style={{fontWeight:'bold'}}>Go Back</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop: 40, flexDirection: 'row' }} onPress={Refresh}>
                    <Icon name='refresh' size={20} />
                    <Text style={{fontWeight:'bold'}}>Refresh</Text>
                </TouchableOpacity>
            </View>
            {loading?
        <ActivityIndicator size='large' color='#000ff'/>
        :
    <FlatList
        style={{height:'100%', marginTop: 20}}
        data={users1}
        numColumns={1}
        renderItem={({item})=>(
          <TouchableOpacity
            onPress={()=>navigation.navigate("BreakfastItem", item)}
            >
                <View style={style.Cart}>
                    <View style={{height: 125, alignItems: 'center'}}>
                        <Image
                            style={{width: 120, height: 130}}
                            source={{uri: item.downloadURL}}/>
                    </View>
                    <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: 30,marginLeft:35}}
                    >
                    {item.top}
                    </Text>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 10,
                        }}>
                        
                       
                    </View>
                </View>
            </TouchableOpacity>
        )}
      />
        }
        </View>
    )
}
const style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center',
    },
    v: {
        marginHorizontal: 20,
        marginTop: 10,
        justifyContent:'center'
    },
    v1:{
        justifyContent: 'center',
    },
    txt:{
        fontSize: 15,
        marginTop: 14,
    },
    header: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    searchContainer: {
        height: 40,
        borderRadius: 10,
        flex: 1,
        backgroundColor: theme.colors.item,
        flexDirection: 'row',
        alignItems: 'center',
    },
    input: {
        fontSize: 18,
        fontWeight: 'bold',
        flex: 1,
        color: 'black',
      },
    sortBtn: {
        marginLeft: 10,
        height: 40,
        width: 40,
        borderRadius: 10,
        backgroundColor: theme.colors.item,
        justifyContent: 'center',
        alignItems: 'center',
      },
      //category
      categoryContainer: {
        flexDirection: 'row',
        marginTop: 15,
        marginBottom: 20,
        justifyContent: 'space-between',
      },
      categoryText: {fontSize: 16, color: 'grey', fontWeight: 'bold'},
      categoryTextSelected: {
        color: theme.colors.start,
        paddingBottom: 5,
        borderBottomWidth: 2,
        borderColor: theme.colors.start,
      },
      //cart
      Cart: {
          height: 225,
          backgroundColor: '#A2B9D1',
          width,
          marginHorizontal: 2,
          borderRadius: 10,
          marginBottom: 20,
          padding: 15,
      }
})








