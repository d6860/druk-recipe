import { StyleSheet,Image, Text, View,SafeAreaView, TouchableOpacity,ScrollView, ImageBackground,Button } from 'react-native'
import React from 'react'

const Add = ({navigation}) => {
  return (
    <View>
      <SafeAreaView style={{maxHeight:'100%'}}>
              
        <ScrollView>
                
           <View>
                
            <View style={styles.container}>
                
                <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.7}} source={require('../../assets/assets/break.png')} resizeMode="cover" style={styles.image}>
                  <Text style={styles.text}>BREAKFAST</Text>
                </ImageBackground>
            </View>

            <View style={styles.container}>
                <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.7}} source={require('../../assets/assets/lunch.jpg')} resizeMode="cover" style={styles.image}>
                  <Text style={styles.text}>LUNCH</Text>
                </ImageBackground>
            </View>

            <View style={styles.container}>
                <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.7}} source={require('../../assets/assets/dinner.jpg')} resizeMode="cover" style={styles.image}>
                  <Text style={styles.text}>DINNER</Text>
                </ImageBackground>
            </View>

            <View style={styles.container}>
                <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.7}} source={require('../../assets/assets/snacks.jpg')} resizeMode="cover" style={styles.image}>
                  <Text style={styles.text}>SNACKS</Text>
                  <TouchableOpacity style={{marginLeft:10, backgroundColor:'black',width:170,marginLeft:200,borderRadius:10,alignItems:'center',justifyContent:'center'}} onPress={()=>navigation.replace("UserLoginScreen")}>
                  </TouchableOpacity >
                </ImageBackground>
            </View>

            <View style={styles.container}>
              <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.6}} source={require('../../assets/assets/drinks.jpg')} resizeMode="cover" style={styles.image}>
                  <Text style={styles.text}>Drinks</Text>
                  <TouchableOpacity style={{backgroundColor:'black',width:100,marginLeft:250,borderRadius:10,marginBottom:30}}>
                      <Button onPress={()=>navigation.replace("UserLoginScreen")} title='AddRecipe'></Button>
                  </TouchableOpacity>
                  <TouchableOpacity style={{backgroundColor:'black',width:100,marginLeft:250,borderRadius:10,}}>
                      <Button title='Search'></Button>
                  </TouchableOpacity>
                </ImageBackground>
            </View>
            
                
            </View>
        </ScrollView>
            

      </SafeAreaView>
    </View>
   
    
  )
}

export default Add

const styles = StyleSheet.create({
  text: {
    color:'white',
    borderRadius:20,
    justifyContent:'center',
    alignItems:'center',
   },
  container:{
    backgroundColor:'black',
    height:50,
    width:200,
    marginTop: 20,
    marginLeft:100,
    alignItems:'center',
    borderRadius:20,
    justifyContent:'center'
  },
  container: {
    marginTop: 10,
  },
   image: {
     flex: 1,
     justifyContent: "center",
     width: '95%',
     height: 200,
     marginLeft:20,
     overflow:'hidden',
  },
   text: {
     color: "black",
     fontSize: 42,
     lineHeight: 84,
     fontWeight: "bold",
     textAlign: "center",
     marginTop:-20
  },
   add:{
     width: 60,
     height: 60,
     marginLeft:300,
     marginTop:100,
  },
   icon:{
     color:'white',
     height:60,
     justifyContent:'center',
     alignItems:'center',
     marginLeft:170,
    paddingTop:20,
    fontSize:20,
  },
   icon1:{
  }
})