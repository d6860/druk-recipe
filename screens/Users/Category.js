import React from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView } from "react-native";
import Action from "../../component/Components/Action";
import { Ionicons } from '@expo/vector-icons'; 
const CommentCategory = ({navigation}) => (
  <SafeAreaView style={{maxHeight:'100%'}}>


    <View style={{backgroundColor:'#2f3d4f', }}>
     
      <View style={{marginTop:25,}}>
        <View style={{marginTop:15,flexDirection:'row',}} >
        <TouchableOpacity onPress={()=>navigation.replace("LoginHomepage")}>
           
           <Ionicons name="md-chevron-back-outline" size={24} color="white" /> 
           </TouchableOpacity>
        <Text style={{marginLeft:130,marginBottom:15, fontSize:20,color:'white'}}>Category</Text>
        </View>
     
      </View>
      
        </View>
        
    
   
      <ScrollView>
     

        
        <View  style={{marginTop:100}}>
        <View style={{}}>
                <Text style={{marginBottom:20,  marginLeft:110,fontSize:20}} > Choose a Category</Text>
        </View>
        
        
          <View style={styles.container}>
            <TouchableOpacity  onPress={()=>navigation.replace("Pick")} >
                <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/break.png')} resizeMode="cover" style={styles.image}>
                  <Text style={{color: "white",
                          fontSize: 20,
                          lineHeight: 84,
                          fontWeight: "bold",
                        marginLeft:80
                          }}>BREAKFAST</Text>
                </ImageBackground>
            </TouchableOpacity>
           
          </View>

          <View style={styles.container}>
            <TouchableOpacity  onPress={()=>navigation.replace("Pick1")}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/lunch.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>LUNCH</Text>
            </ImageBackground>
            </TouchableOpacity>
            
          </View>

          <View style={styles.container}>
            <TouchableOpacity onPress={()=>navigation.replace("Pick2")}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/dinner.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>DINNER</Text>
            </ImageBackground>
            </TouchableOpacity>
           
          </View>

          <View style={styles.container}>
            <TouchableOpacity  onPress={()=>navigation.replace("Pick3")} >
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9, blurRadius:1}} source={require('../../assets/assets/snacks.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>SNACKS</Text>
            </ImageBackground>
            </TouchableOpacity>
            
          </View>

          <View style={styles.container}>
            <TouchableOpacity onPress={()=>navigation.replace("Pick4")}>
            <ImageBackground imageStyle={{borderRadius:15, position:'absolute',opacity:0.9}} source={require('../../assets/assets/drinks.jpg')} resizeMode="cover" style={styles.image}>
              <Text style={styles.text}>Drinks</Text>
              
            </ImageBackground>
            </TouchableOpacity>
            
          </View>

          
        </View>
      </ScrollView>

      
            {/* <View>
              <TouchableOpacity>
                <Image style={{width:30,height:30,marginLeft:40,marginTop:10}} source={require('../../assets/assets/h.png')}/>
                <Text style={{marginLeft:35,marginBottom:30}}>Home</Text>
                
              </TouchableOpacity>
            </View>

            <View>
              <TouchableOpacity  onPress={()=>navigation.replace("UserLoginScreen")}>
                <Image style={{width:30,height:30,marginLeft:90,marginTop:10}} source={require('../../assets/assets/a.png')} ></Image>
                <Text style={{marginLeft:80,}}>About</Text>
              </TouchableOpacity>
            </View>

            <View>
              <TouchableOpacity onPress={()=>navigation.replace("AddPage")}>
                
                
                           </TouchableOpacity>
            </View> */}
        
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
   marginTop: 7,
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '80%',
    height: 80,
    marginLeft:20,
    overflow:'hidden',
    marginLeft:73
  },
  text: {
    color: "white",
    fontSize: 20,
    lineHeight: 84,
    fontWeight: "bold",
   marginLeft:100
    
    
  },
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  
});

export default  CommentCategory ;