import React,{useState,useEffect} from "react";
import { ImageBackground, StyleSheet, Text, TouchableOpacity,Image, View, ScrollView,SafeAreaView, FlatList } from "react-native";
import Action from "../../component/Components/Action";
import {firebase} from '../../component/Firebase/config';
import { Ionicons } from '@expo/vector-icons'; 
export default function UserBreakfast ({navigation}) {
  // const [tittle, setTittle] = useState(''); 
  const [breakfast, setBreakfast] = useState([]);
  const tittleRef = firebase.firestore().collection('breakfast'); 

  useEffect(() => {

    tittleRef
        .orderBy('createdAt', 'desc')
        // .where ('username','==','Domtu')
        .onSnapshot(
            querySnapshot => {
                const newBreakfast = []
                querySnapshot.forEach(doc => {
                    const tittle = doc.data()
                    tittle.id = doc.id
                    newBreakfast.push(tittle)
                });
                setBreakfast(newBreakfast)
            },
            error => {

                console.error(error);
            }
        )
}, []);

const renderTittle = ({ item }) => {
  return (

      <View style={styles.tittleContainer} >
            
              <Text style={styles.tittleText}>
                  {item.text[0].toUpperCase() + item.text.slice(1)}
              </Text>
          

      </View>
  )
}
return (

  <SafeAreaView>
    <View style={{backgroundColor:'#2f3d4f', width:"100%" }}>
     
     <View style={{marginTop:2,}}>
       <View style={{marginTop:15,flexDirection:'row',marginLeft:'5%'}} >
       <TouchableOpacity onPress={()=>navigation.replace("Page")}>
          
          <Ionicons name="md-chevron-back-outline" size={24} color="white" /> 
          </TouchableOpacity>
       <Text style={{marginLeft:90,marginBottom:15, fontSize:20,color:'white'}}>Reviews</Text>
       </View>
    
     </View>
     
       </View>
    
    <View style={{height:'100%'}}>

    {breakfast.length > 0 && (
              <View style={styles.listContainer}>
                  <FlatList
                      data={breakfast}
                      // numColumns={2}
                      horizontal={false}
                      ItemSeparatorComponent={() => <View style={{margin: 4}}/>}
                      renderItem={({ item }) => {
                        return(
                          
                           
                           <View key={item.id} style={styles.place} >
                           <Text style={{marginTop:20, color:'white',marginLeft:10,fontSize:15}}>{item.comment}</Text>
                           
                          
                           </View>
                           
                         )
                       }} />
                
              </View>
          )}
    </View>
    <View>
    
    </View>
  </SafeAreaView>
)
}

const styles = StyleSheet.create({
  container: {
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '95%',
    height: 200,
    marginLeft:20,
    overflow:'hidden',
  },
  text: {
    color: "white",
    fontSize: 28,
    lineHeight: 84,
    fontWeight: "bold",
    marginTop:130
    
  },
  add:{
    width: 60,
    height: 60,
    marginLeft:300,
    marginTop:100,
  },
  icon:{
    color:'white',
    height:60,
    justifyContent:'center',
    alignItems:'center',
    marginLeft:170,
   paddingTop:20,
   fontSize:20,
  },
  tittleContainer: {
    borderBottomColor: 'black',
    borderWidth: 1,
    flex:1 ,
    flexDirection: 'column',
    height: 100,
    width: 100,
    margin: 5,
    backgroundColor: 'blue'
  },
  tittleText: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  listContainer: {
    height: '90%',
    width: 400,
    flexDirection: 'row',
   marginTop:30
    
    
  },
  place:{
   width:'80%',
   height:'50%',
   backgroundColor:'#2f3d4f',
   marginLeft:20,
   borderRadius:10,
   
  }
  
});



