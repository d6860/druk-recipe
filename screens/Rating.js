import React, {useState} from 'react';
 
// import react-native components
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground
} from 'react-native';
import { Ionicons } from '@expo/vector-icons'; 
const Rating = ({navigation}) => {
  // Set the default Ratings Selected
  const [defaultRating, setDefaultRating] = useState();
  // Set the max number of Ratings
  const [maxRating, setMaxRating] = useState([1, 2, 3, 4, 5]);
 
  // Filled Star
  const starImageFilled =
    'https://www.techup.co.in/wp-content/uploads/2020/11/ic_star_fill.png';
  // Empty Star
  const starImageCorner =
    'https://www.techup.co.in/wp-content/uploads/2020/11/ic_star.png';
  // Half Star
  const startHalfFilled =
    'https://www.techup.co.in/wp-content/uploads/2020/11/ic_star_half.png';
 
  const onStarClick = (item, bool) => {
    if (bool) {
      item = item - 1 + 0.5;
    }
    setDefaultRating(item);
  };
 
  const CustomRatingBar = () => {
    
    return (
      
      <View style={styles.ratingBarStyle}>
        
        {maxRating.map((item, key) => {
          return (
            <View>
              <Image
                style={styles.imageStyle}
                source={
                  item <= defaultRating
                    ? {uri: starImageFilled}
                    : item >= defaultRating && item < defaultRating + 1
                    ? {uri: startHalfFilled}
                    : {uri: starImageCorner}
                }
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  position: 'absolute',
                }}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={{
                    width: 20,
                    height: 40,
                  }}
                  onPress={() => onStarClick(item, true)}
                />
 
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={{
                    width: 20,
                    height: 40,
                  }}
                  onPress={() => onStarClick(item, false)}
                />
              </View>
            </View>
          );
        })}
      </View>
    );
  };
 
  return (
    
    <SafeAreaView style={styles.container}>
     <View style={{backgroundColor:'#2f3d4f', width:"100%" }}>
     
     <View style={{marginTop:2,}}>
       <View style={{marginTop:15,flexDirection:'row',marginLeft:'5%'}} >
       <TouchableOpacity onPress={()=>navigation.replace("Page")}>
          
          <Ionicons name="md-chevron-back-outline" size={24} color="white" /> 
          </TouchableOpacity>
       <Text style={{marginLeft:90,marginBottom:15, fontSize:20,color:'white'}}>Rating</Text>
       </View>
    
     </View>
     
       </View>
      
       
      <View style={styles.container}>
      <View style={{marginBottom:150}}>
      {/* <ImageBackground imageStyle={{width:400,height:300,marginLeft:0,}} source={require('../assets/assets/rate.png')}/> */}
         
      </View>
     
        <Text style={styles.titleText}>Give rating for breakfast</Text>
        {/* Custom Rating Bar component */}
        <CustomRatingBar />
        <Text style={styles.textStyle}>
          {/* Display selected Ratings */}
          {defaultRating} / {Math.max.apply(null, maxRating)}
        </Text>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.buttonStyle}
          onPress={()=>navigation.replace("Page")}>
          {/* Button to display selected Ratings in alert box */}
          <Text style={styles.buttonTextStyle}>Submit</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
 
export default Rating;
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
    justifyContent: 'center',
    textAlign: 'center',
    marginBottom:100,
  
  },
  titleText: {
    padding: 8,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  textStyle: {
    textAlign: 'center',
    fontSize: 23,
    color: '#000',
    marginTop: 15,
  },
  textStyleSmall: {
    textAlign: 'center',
    fontSize: 16,
    color: '#000',
    marginTop: 15,
  },
  buttonStyle: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 30,
    padding: 15,
    backgroundColor: '#2f3d4f',
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
  },
  ratingBarStyle: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 30,
  },
  imageStyle: {
    width: 40,
    height: 40,
    resizeMode: 'cover',
  },
});