import { StyleSheet, Text, View, Image  } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { usestate } from "react";
// import RegistrationScreen from "./src/screens/RegistrationScreen/RegistrationScreen";
// import LoginScreen from "./src/screens/Users/UserLoginScreen/UserLoginScreen";
// import { HomePage } from "./src/screens/ResetScreen";
// import ResetPasswordScreen from "./src/screens/ResetScreen/ResetPasswordScreen";
// import {  ResetScreen, HomeScreen } from "./src/screens/ResetScreen";
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
// import DrawerContent from './src/Components/DrawerContent';
// import addRecipe from "./src/screens/ResetScreen/AddRecipe";
// import First from "./src/screens/ResetScreen/FirstPage"
import DrawerContent from './component/Components/DrawerContent';
import UserRegistrationScreen from "../users/screens/Users/UserRegistrationScreen/UserRegistrationScreen";
import UserLoginScreen from "../users/screens/Users/UserLoginScreen/UserLoginScreen";
import UserResetPasswordScreen from "../users/screens/Users/UserResetScreen/UserResetPasswordScreen";

import Page from './screens/Users/UserHomepage'
import Add from './screens/Users/AddPage'
import AddRecipe  from "./screens/Users/UserAddRecipe";
import Update from "../users/screens/Users/Update"
import LoginHomepage from "../users/screens/Users/LoginHomepage"
import AboutPage from "../users/screens/Users/AboutPage"
import AddBreakfast from "./screens/Users/AddBreakfast"
import AddLunch from "./screens/Users/AddLunch"
import AddDinner from "./screens/Users/AddDinner"
import AddSnacks from "./screens/Users/AddSnacks"
import AddDrinks from "./screens/Users/AddDrinks"
import Category from "./screens/Users/Category";
import BreakFast from "./screens/Users/BreakFast";
import UserBreakFast from "./screens/Users/UserBreakFast";
import Action  from "./component/Components/Action"
import BreakfastItem from './screens/Users/BreakfastItem'
import UserBreakfastItem from './screens/Users/UserBreakfastItem'
import UserBreakfastItem1 from './screens/Users/UserBreakfastItem1'
import Lunch from './screens/Users/Lunch'
import UserLunch from './screens/Users/UserLunch'
import LunchItem from './screens/Users/LunchItem'
import LunchItem1 from './screens/Users/LunchItem1'
import UserLunchItem from './screens/Users/UserLunchItem'
import UserLunchItem1 from './screens/Users/UserLunchItem1'
import Dinner from './screens/Users/Dinner'
import UserDinner from './screens/Users/UserDinner'
import DinnerItem from './screens/Users/DinnerItem'
import DinnerItem1 from './screens/Users/DinnerItem1'
import UserDinnerItem from './screens/Users/UserDinnerItem'
import UserDrinksItem1 from './screens/Users/UserDrinksItem1'
import Snacks from './screens/Users/Snacks'
import UserSnacks from './screens/Users/UserSnacks'
import SnacksItem from './screens/Users/SnacksItem'
import SnacksItem1 from './screens/Users/SnacksItem1'
import UserSnacksItem from './screens/Users/UserSnacksItem'
import Drinks from './screens/Users/Drinks'
import UserDrinks from './screens/Users/UserDrinks'
import DrinksItem from './screens/Users/DrinksItem'
import DrinksItem1 from './screens/Users/DrinksItem1'
import UserDrinksItem from './screens/Users/UserDrinksItem'
import User from './screens/Users/User'
import CommentCategory from './screens/CommentCategory'
import BreakfastComment from './screens/BreakfastComment'
import LunchComment from './screens/LunchComment'
import DinnerComment from './screens/DinnerComment'
import SnacksComment from './screens/SnacksComment'
import DrinksComment from './screens/DrinksComment'
import Rating from './screens/Rating'
import Logo from './screens/Logo'
import Search from './screens/Users/Search'
import Save from './screens/Save'
import Pick from './screens/Pick'
import Save1 from './screens/Save1'
import Pick1 from './screens/Pick1'
import Pick2 from './screens/Pick2'
import Pick3 from './screens/Pick3'
import Pick4
 from './screens/Pick4'
import BreakfastItem1 from './screens/Users/BreakfastItem1'
const Stack = createNativeStackNavigator();
 const Drawer =createDrawerNavigator();


export default function App() { 
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Page'
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="UserLoginScreen" component={UserLoginScreen} />      
        <Stack.Screen name="UserRegistrationScreen" component={UserRegistrationScreen} />
        <Stack.Screen name="ResetPasswordScreen" component={UserResetPasswordScreen} /> 
        <Stack.Screen name="Page" component={Page} />
        {/* <Stack.Screen name="AddPage" component={Add} /> */}
        <Stack.Screen name="UserAddRecipe" component={AddRecipe} />
        <Stack.Screen name="userUpdate" component={Update} />
        <Stack.Screen name="LoginHomepage" component={DrawerNavigator} />
        <Stack.Screen name="AboutPage" component={AboutPage} />
        <Stack.Screen name="AddBreakfast" component={AddBreakfast} />
        <Stack.Screen name="AddLunch" component={AddLunch} />
        <Stack.Screen name="AddDinner" component={AddDinner} />
        <Stack.Screen name="AddSnacks" component={AddSnacks} />
        <Stack.Screen name="AddDrinks" component={AddDrinks} />
        <Stack.Screen name="Category" component={Category} />
        <Stack.Screen name="BreakFast" component={BreakFast} />
        <Stack.Screen name="UserBreakFast" component={UserBreakFast} />
        <Stack.Screen name="BreakfastItem" component={BreakfastItem} />
        <Stack.Screen name="UserBreakfastItem" component={UserBreakfastItem} />
        <Stack.Screen name="UserBreakfastItem1" component={UserBreakfastItem1} />
        <Stack.Screen name="Lunch" component={Lunch} />
        <Stack.Screen name="UserLunch" component={UserLunch} />
        <Stack.Screen name="LunchItem" component={LunchItem} />
        <Stack.Screen name="LunchItem1" component={LunchItem1} />
        <Stack.Screen name="UserLunchItem" component={UserLunchItem} />
        <Stack.Screen name="UserLunchItem1" component={UserLunchItem1} />
        <Stack.Screen name="Dinner" component={Dinner} />
        <Stack.Screen name="UserDinner" component={UserDinner} />
        <Stack.Screen name="DinnerItem" component={DinnerItem} />
        <Stack.Screen name="DinnerItem1" component={DinnerItem1} />
        <Stack.Screen name="UserDinnerItem" component={UserDinnerItem} />
        <Stack.Screen name="Snacks" component={Snacks} />
        <Stack.Screen name="UserSnacks" component={UserSnacks} />
        <Stack.Screen name="SnacksItem" component={SnacksItem} />
        <Stack.Screen name="SnacksItem1" component={SnacksItem1} />
        <Stack.Screen name="UserSnacksItem" component={UserSnacksItem} />
        <Stack.Screen name="Drinks" component={Drinks} />
        <Stack.Screen name="UserDrinks" component={UserDrinks} />
        <Stack.Screen name="DrinksItem" component={DrinksItem} />
        <Stack.Screen name="DrinksItem1" component={DrinksItem1} />
        <Stack.Screen name="UserDrinksItem" component={UserDrinksItem} />
        <Stack.Screen name="UserDrinksItem1" component={UserDrinksItem1} />
        <Stack.Screen name="User" component={User} />
        <Stack.Screen name="BreakfastComment" component={BreakfastComment} />
        <Stack.Screen name="CommentCategory" component={CommentCategory} />
        <Stack.Screen name="LunchComment" component={LunchComment} />
        <Stack.Screen name="DinnerComment" component={DinnerComment} />
        <Stack.Screen name="SnacksComment" component={SnacksComment} />
        <Stack.Screen name="DrinksComment" component={DrinksComment} />
        <Stack.Screen name="Rating" component={Rating} />
        <Stack.Screen name="Logo" component={Logo} />
        <Stack.Screen name="Save" component={Save} />
        <Stack.Screen name="Pick" component={Pick} />
        <Stack.Screen name="Save1" component={Save1} />
        <Stack.Screen name="Pick1" component={Pick1} />
        <Stack.Screen name="Pick2" component={Pick2} />
        <Stack.Screen name="Pick3" component={Pick3} />
        <Stack.Screen name="Pick4" component={Pick4} />
        <Stack.Screen name="BreakfastItem1" component={BreakfastItem1} />
        <Stack.Screen name="Search" component={Search} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
const DrawerNavigator =() =>{
  return(
    
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='Home' component={LoginHomepage}/>
      

    </Drawer.Navigator>
    
    
    
  )
}

