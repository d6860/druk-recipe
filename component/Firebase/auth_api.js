import {firebase} from './config'
import "firebase/auth"
import { getAuth, sendPasswordResetEmail, signOut } from "firebase/auth";

export function logoutUser(){
    firebase.auth().signOut();
    
}

export  function resetPassword(email){
    try{
         firebase
            .auth()
            .sendPasswordResetEmail(email);
            console.log('yes')
        return {};
    }
    catch  (error){
        return{
            error: error.message,
        }
    }
}
