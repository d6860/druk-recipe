import React, { useState, useEffect } from 'react';
import { Image, View, Platform, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
export default function UploadImage() {

 return (

    <View>
   
        <TouchableOpacity onPress={()=>navigation.replace("Adds")} >
            <Text>Image</Text>
            
        </TouchableOpacity>
    </View>
 );
}
const imageUploaderStyles=StyleSheet.create({
   container:{
       elevation:2,
       height:70,
       width:120,
       backgroundColor:'#EFEFEF',
       position:'relative',
    //    borderRadius:888,
       overflow:'hidden', 
       marginLeft:40,
       marginTop:20
   },
   uploadBtnContainer:{
       opacity:0.7,
       position:'absolute',
       right:0,
       bottom:0,
       backgroundColor:'blue',
       width:'100%',
       height:'50%',
   },
   uploadBtn:{
       display:'flex',
       alignItems:"center",
       justifyContent:'center'
   }
})